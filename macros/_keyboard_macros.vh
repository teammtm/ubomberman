//////////////////////////////////////////////////////////////////////////////////
// Company: AGH UST
// Engineer: Dawid Włodarczyk
// 
// Create Date: 01.06.2018 12:51:18
// Design Name: uBomberman
// Header Name: _keyboard_macros
// Project Name: uec2_project
// Description: Macros for keyboard bus
// 
// Dependencies: 
// none
//
// Revision:
// - Rev. 1.0   -DW- File Created
// 
//////////////////////////////////////////////////////////////////////////////////

`ifndef _keyboard_macros
`define _keyboard_macros

//Keybord key codes definitions
`define W       8'h1D
`define A       8'h1C
`define S       8'h1B
`define D       8'h23
`define LSHIFT  8'h12
`define I       8'h43
`define J       8'h3B
`define K       8'h42
`define L       8'h4B
`define RSHIFT  8'h59
`define ENTER   8'h5A
`define ESC     8'h76
`define UP      8'h75
`define DOWN    8'h72

`define KEY_RELEASED 8'hF0

//keyboard bus sizes
`define KB_BUS_SIZE      14
`define KB_PONE_BUS_SIZE 5
`define KB_PTWO_BUS_SIZE 5
`define KB_MENU_BUS_SIZE 4

//bus components
`define KB_PONE_UP      `W
`define KB_PONE_DOWN    `S
`define KB_PONE_LEFT    `A
`define KB_PONE_RIGHT   `D
`define KB_PONE_BOMB    `LSHIFT
`define KB_PTWO_UP      `I
`define KB_PTWO_DOWN    `K
`define KB_PTWO_LEFT    `J
`define KB_PTWO_RIGHT   `L
`define KB_PTWO_BOMB    `RSHIFT
`define KB_MENU_CANCEL  `ESC
`define KB_MENU_OK      `ENTER
`define KB_MENU_UP      `UP
`define KB_MENU_DOWN    `DOWN

`define KB_PONE_BITS        4:0
`define KB_PONE_UP_BITS     0
`define KB_PONE_DOWN_BITS   1
`define KB_PONE_LEFT_BITS   2
`define KB_PONE_RIGHT_BITS  3
`define KB_PONE_BOMB_BITS   4

`define KB_PTWO_BITS        9:5
`define KB_PTWO_UP_BITS     0
`define KB_PTWO_DOWN_BITS   1
`define KB_PTWO_LEFT_BITS   2
`define KB_PTWO_RIGHT_BITS  3
`define KB_PTWO_BOMB_BITS   4

`define KB_MENU_BITS        13:10
`define KB_MENU_CANCEL_BITS 0
`define KB_MENU_OK_BITS     1
`define KB_MENU_UP_BITS     2
`define KB_MENU_DOWN_BITS   3
                            
//keyboard bus split at input
`define KB_SPLIT_INPUT(BUS_NAME) \
    wire [`KB_PONE_BUS_SIZE-1:0] pone_in = BUS_NAME[`KB_PONE_BITS]; \
    wire [`KB_PTWO_BUS_SIZE-1:0] ptwo_in = BUS_NAME[`KB_PTWO_BITS]; \
    wire [`KB_MENU_BUS_SIZE-1:0] menu_in = BUS_NAME[`KB_MENU_BITS]; 

`define KB_SPLIT_PONE(BUS_NAME) \
    wire pone_up_in = BUS_NAME[`KB_PONE_UP_BITS]; \
    wire pone_down_in = BUS_NAME[`KB_PONE_DOWN_BITS]; \
    wire pone_left_in = BUS_NAME[`KB_PONE_LEFT_BITS]; \
    wire pone_right_in = BUS_NAME[`KB_PONE_RIGHT_BITS]; \
    wire pone_bomb_in = BUS_NAME[`KB_PONE_BOMB_BITS]; 

`define KB_SPLIT_PTWO(BUS_NAME) \
    wire ptwo_up_in = BUS_NAME[`KB_PTWO_UP_BITS]; \
    wire ptwo_down_in = BUS_NAME[`KB_PTWO_DOWN_BITS]; \
    wire ptwo_left_in = BUS_NAME[`KB_PTWO_LEFT_BITS]; \
    wire ptwo_right_in = BUS_NAME[`KB_PTWO_RIGHT_BITS]; \
    wire ptwo_bomb_in = BUS_NAME[`KB_PTWO_BOMB_BITS]; 

`define KB_SPLIT_MENU(BUS_NAME) \
    wire menu_up_in = BUS_NAME[`KB_MENU_UP_BITS]; \
    wire menu_down_in = BUS_NAME[`KB_MENU_DOWN_BITS]; \
    wire menu_ok_in = BUS_NAME[`KB_MENU_OK_BITS]; \
    wire menu_cancel_in = BUS_NAME[`KB_MENU_CANCEL_BITS];  
    
//keyboard bus output variables
`define KB_OUT_WIRE \
    wire [`KB_PONE_BUS_SIZE-1:0] pone_out; \
    wire [`KB_PTWO_BUS_SIZE-1:0] ptwo_out; \
    wire [`KB_MENU_BUS_SIZE-1:0] menu_out; 
    
`define KB_PONE_OUT_WIRE \
    wire                         pone_up_out; \
    wire                         pone_down_out; \
    wire                         pone_left_out; \
    wire                         pone_right_out; \
    wire                         pone_bomb_out; 
    
`define KB_PTWO_OUT_WIRE \
    wire                         ptwo_up_out; \
    wire                         ptwo_down_out; \
    wire                         ptwo_left_out; \
    wire                         ptwo_right_out; \
    wire                         ptwo_bomb_out; 
    
`define KB_MENU_OUT_WIRE \
    wire                         menu_cancel_out; \
    wire                         menu_ok_out; \
    wire                         menu_up_out; \
    wire                         menu_down_out;

`define KB_OUT_REG \
    reg [`KB_PONE_BUS_SIZE-1:0] pone_out; \
    reg [`KB_PTWO_BUS_SIZE-1:0] ptwo_out; \
    reg [`KB_MENU_BUS_SIZE-1:0] menu_out; 
    
`define KB_NXT_REG \
    reg [`KB_PONE_BUS_SIZE-1:0] pone_nxt; \
    reg [`KB_PTWO_BUS_SIZE-1:0] ptwo_nxt; \
    reg [`KB_MENU_BUS_SIZE-1:0] menu_nxt; 
 
`define KB_PONE_OUT_REG \
    reg                         pone_up_out; \
    reg                         pone_down_out; \
    reg                         pone_left_out; \
    reg                         pone_right_out; \
    reg                         pone_bomb_out; 
    
`define KB_PTWO_OUT_REG \
    reg                         ptwo_up_out; \
    reg                         ptwo_down_out; \
    reg                         ptwo_left_out; \
    reg                         ptwo_right_out; \
    reg                         ptwo_bomb_out; 

`define KB_MENU_OUT_REG \
    reg                         menu_cancel_out; \
    reg                         menu_ok_out; \
    reg                         menu_up_out; \
    reg                         menu_down_out;

    
//keyboard bus merge at output
`define KB_MERGE_PONE(BUS_NAME) \
    assign BUS_NAME[`KB_PONE_UP_BITS] = pone_up_out; \
    assign BUS_NAME[`KB_PONE_DOWN_BITS] = pone_down_out; \
    assign BUS_NAME[`KB_PONE_LEFT_BITS] = pone_left_out; \
    assign BUS_NAME[`KB_PONE_RIGHT_BITS] = pone_right_out; \
    assign BUS_NAME[`KB_PONE_BOMB_BITS] = pone_bomb_out;

`define KB_MERGE_PTWO(BUS_NAME) \
    assign BUS_NAME[`KB_PTWO_UP_BITS] = ptwo_up_out; \
    assign BUS_NAME[`KB_PTWO_DOWN_BITS] = ptwo_down_out; \
    assign BUS_NAME[`KB_PTWO_LEFT_BITS] = ptwo_left_out; \
    assign BUS_NAME[`KB_PTWO_RIGHT_BITS] = ptwo_right_out; \
    assign BUS_NAME[`KB_PTWO_BOMB_BITS] = ptwo_bomb_out; 

`define KB_MERGE_MENU(BUS_NAME) \
    assign BUS_NAME[`KB_MENU_CANCEL_BITS] = menu_cancel_out; \
    assign BUS_NAME[`KB_MENU_OK_BITS] = menu_ok_out; \
    assign BUS_NAME[`KB_MENU_UP_BITS] = menu_up_out; \
    assign BUS_NAME[`KB_MENU_DOWN_BITS] = menu_down_out;    

`define KB_MERGE_OUTPUT(BUS_NAME) \
    assign BUS_NAME[`KB_PONE_BITS] = pone_out; \
    assign BUS_NAME[`KB_PTWO_BITS] = ptwo_out; \
    assign BUS_NAME[`KB_MENU_BITS] = menu_out; 

`endif
