//////////////////////////////////////////////////////////////////////////////////
// Company:     AGH UST
// Author:      DW
// 
// Create Date: 06.04.2018 18:30:42
// Design Name: uBomberman
// File Name: _vga_macros
// Project Name: uec2_project
// Description: This is the header file for modules using VGA signals.
// 
// Dependencies: 
// none 
//
// Revisions:
// - Rev. 1.0   -DW- File Created
// 
//////////////////////////////////////////////////////////////////////////////////

`ifndef _vga_macros
`define _vga_macros

//VESA 800x600 @60Hz screen and timing parameters
`define SCREEN_WIDTH 800
`define SCREEN_HEIGTH 600
`define H_MAX_VALUE 1055
`define H_BLNK_START 800
`define H_SYNC_START 840
`define H_SYNC_END 967
`define V_MAX_VALUE 627
`define V_BLNK_START 600
`define V_SYNC_START 601
`define V_SYNC_END 604

//VGA bus sizes
`define VGA_BUS_SIZE 26
`define VGA_HCOUNT_SIZE 11
`define VGA_VCOUNT_SIZE 11

//VGA bus components
`define VGA_HBLNK_BITS 25
`define VGA_VBLNK_BITS 24
`define VGA_HSYNC_BITS 23
`define VGA_VSYNC_BITS 22
`define VGA_HCOUNT_BITS 21:11
`define VGA_VCOUNT_BITS 10:0

//VGA bus split at input
`define VGA_SPLIT_INPUT(BUS_NAME) \
    wire hsync_in = BUS_NAME[`VGA_HSYNC_BITS]; \
    wire vsync_in = BUS_NAME[`VGA_VSYNC_BITS]; \
    wire hblnk_in = BUS_NAME[`VGA_HBLNK_BITS]; \
    wire vblnk_in = BUS_NAME[`VGA_VBLNK_BITS]; \
    wire [`VGA_HCOUNT_SIZE-1:0] hcount_in = BUS_NAME[`VGA_HCOUNT_BITS]; \
    wire [`VGA_VCOUNT_SIZE-1:0] vcount_in = BUS_NAME[`VGA_VCOUNT_BITS]; 

//VGA bus output variables
`define VGA_OUT_WIRE \
    wire hsync_out; \
    wire vsync_out; \
    wire hblnk_out; \
    wire vblnk_out; \
    wire [`VGA_HCOUNT_SIZE-1:0] hcount_out; \
    wire [`VGA_VCOUNT_SIZE-1:0] vcount_out;
    
`define VGA_OUT_REG \
    reg hsync_out; \
    reg vsync_out; \
    reg hblnk_out; \
    reg vblnk_out; \
    reg [`VGA_HCOUNT_SIZE-1:0] hcount_out; \
    reg [`VGA_VCOUNT_SIZE-1:0] vcount_out;

//VGA bus merge at output
`define VGA_MERGE_OUTPUT(BUS_NAME) \
    assign BUS_NAME[`VGA_HSYNC_BITS] = hsync_out; \
    assign BUS_NAME[`VGA_VSYNC_BITS] = vsync_out; \
    assign BUS_NAME[`VGA_HBLNK_BITS] = hblnk_out; \
    assign BUS_NAME[`VGA_VBLNK_BITS] = vblnk_out; \
    assign BUS_NAME[`VGA_HCOUNT_BITS] = hcount_out; \
    assign BUS_NAME[`VGA_VCOUNT_BITS] = vcount_out; 

`endif