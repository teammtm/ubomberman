//////////////////////////////////////////////////////////////////////////////////
// Company:     AGH UST
// Author:      DW
// 
// Create Date: 16.03.2018 22:41:13
// Design Name: uBomberman
// Module Name: top_ubomberman
// Project Name: uec2_project
// Description: This is the top module for uBomberman game design.
// 
// Dependencies: 
// - clk_module, _vga_macros, vga_timing, keyboard_ctrl
//
// Revisions:
// - Rev. 1.0   -DW-            File Created
// - Rev. 1.01  -DW- 06.04.18   Added input and output ports
//                              Connected vga_ctl module
//////////////////////////////////////////////////////////////////////////////////

`timescale 1ns / 1ps
`include "_vga_macros.vh"
`include "_keyboard_macros.vh"

module top_ubomberman(
    input wire clk,
    input wire rst,
    input wire PS2Clk,
    input wire PS2Data,
    output reg vgaVsync,
    output reg vgaHsync,
    output reg [3:0] vgaRed,
    output reg [3:0] vgaGreen,
    output reg [3:0] vgaBlue,
    output wire [13:0] led
    );
    
    wire clk_100MHz;
    wire clk_50MHz;
    wire clk_40MHz;
    
    clk_module u_clk_module(
        .clk_in(clk),
        .clk_100MHz(clk_100MHz),
        .clk_50MHz(clk_50MHz),
        .clk_40MHz(clk_40MHz)
    );
    
    wire [`KB_BUS_SIZE-1:0] kb_bus;
    
    keyboard_ctrl u_keyboard_ctrl(
        .clk(clk_100MHz),
        .clk50MHz(clk_40MHz),
        .rst(rst),
        .PS2Data(PS2Data),
        .PS2Clk(PS2Clk),
        .kb_bus_out(led)
    );
    
    wire [`VGA_BUS_SIZE-1:0] vga_bus [1:0];
    
    vga_timing u_vga_timing(
        .clk(clk_40MHz),
        .rst(rst),
        .vga_out(vga_bus[0])
    );
    
   wire [11:0] rgb;
   wire [11:0] rgb_pixel;
   wire [14:0] pixel_addr;
     
     draw_startscreen u_draw_startscreen(
         .clk(clk_40MHz),
         .rst(rst),
         .vga_in(vga_bus[0]),
         .vga_out(vga_bus[1]),
         .rgb_pixel(rgb_pixel),
         .rgb_in(12'hd_d_d),
         .rgb_out(rgb),
         .pixel_addr(pixel_addr)
     );
     
     startscreen_rom u_startscreen_rom(
         .clk(clk_40MHz),
         .address(pixel_addr),
         .rgb(rgb_pixel)
     );

     `VGA_SPLIT_INPUT(vga_bus[1])
     
     reg        hs_nxt,vs_nxt;
     reg [11:0] rgb_nxt;
     
     //blank inactive screen area
     always @(posedge clk_40MHz)
             begin
                 vgaHsync <= hs_nxt;
                 vgaVsync <= vs_nxt;
                 {vgaRed,vgaGreen,vgaBlue} <= rgb_nxt;
             end
             
     always @*
         if(rst)
             begin
                 hs_nxt  = 0;
                 vs_nxt  = 0;
                 rgb_nxt = 0;
             end
         else
             begin
                 hs_nxt = hsync_in;
                 vs_nxt = vsync_in;
                 if (vblnk_in || hblnk_in)
                     rgb_nxt = 12'h0_0_0; 
                 else
                     rgb_nxt = rgb;
             end
endmodule
