`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: AGH UST
// Engineer: Dawid Włodarczyk
// 
// Create Date: 01.06.2018 14:33:41
// Design Name: uBomberman
// Module Name: decode_keycode
// Project Name: uec2_project
// Description: Decode keyboard keycode to separated outputs.
// 
// Dependencies: 
// none
//
// Revision:
// - Rev. 1.0   -DW- File Created
// 
//////////////////////////////////////////////////////////////////////////////////

`include "_keyboard_macros.vh"

module decode_keycode(
    input wire                     clk,
    input wire                     rst,
    input wire  [15:0]             keycode,
    input wire                     flag,
    output wire [`KB_BUS_SIZE-1:0] kb_bus_out
    );
    
    `KB_OUT_REG
    `KB_NXT_REG
    `KB_MERGE_OUTPUT(kb_bus_out)
    
    always@(posedge clk)
        if(rst) begin
            pone_out <= 0;
            ptwo_out <= 0;
            menu_out <= 0;
        end
        else begin
            pone_out <= pone_nxt;
            ptwo_out <= ptwo_nxt;
            menu_out <= menu_nxt;
        end
       
    always @* 
    begin
        pone_nxt = pone_out;
        ptwo_nxt = ptwo_out;
        menu_nxt = menu_out;
        if (flag) begin
            casex(keycode)
                {`KEY_RELEASED,`KB_PONE_UP    }: pone_nxt[`KB_PONE_UP_BITS    ] = 1'b0;
                {`KEY_RELEASED,`KB_PONE_DOWN  }: pone_nxt[`KB_PONE_DOWN_BITS  ] = 1'b0;
                {`KEY_RELEASED,`KB_PONE_LEFT  }: pone_nxt[`KB_PONE_LEFT_BITS  ] = 1'b0;
                {`KEY_RELEASED,`KB_PONE_RIGHT }: pone_nxt[`KB_PONE_RIGHT_BITS ] = 1'b0;
                {`KEY_RELEASED,`KB_PONE_BOMB  }: pone_nxt[`KB_PONE_BOMB_BITS  ] = 1'b0;
                {`KEY_RELEASED,`KB_PTWO_UP    }: ptwo_nxt[`KB_PTWO_UP_BITS    ] = 1'b0;
                {`KEY_RELEASED,`KB_PTWO_DOWN  }: ptwo_nxt[`KB_PTWO_DOWN_BITS  ] = 1'b0;
                {`KEY_RELEASED,`KB_PTWO_LEFT  }: ptwo_nxt[`KB_PTWO_LEFT_BITS  ] = 1'b0;
                {`KEY_RELEASED,`KB_PTWO_RIGHT }: ptwo_nxt[`KB_PTWO_RIGHT_BITS ] = 1'b0;
                {`KEY_RELEASED,`KB_PTWO_BOMB  }: ptwo_nxt[`KB_PTWO_BOMB_BITS  ] = 1'b0;       
                {`KEY_RELEASED,`KB_MENU_CANCEL}: menu_nxt[`KB_MENU_CANCEL_BITS] = 1'b0;
                {`KEY_RELEASED,`KB_MENU_OK    }: menu_nxt[`KB_MENU_OK_BITS    ] = 1'b0;
                {`KEY_RELEASED,`KB_MENU_UP    }: menu_nxt[`KB_MENU_UP_BITS    ] = 1'b0;
                {`KEY_RELEASED,`KB_MENU_DOWN  }: menu_nxt[`KB_MENU_DOWN_BITS  ] = 1'b0;
        
                {8'hxx,`KB_PONE_UP    }: pone_nxt[`KB_PONE_UP_BITS    ] = 1'b1;
                {8'hxx,`KB_PONE_DOWN  }: pone_nxt[`KB_PONE_DOWN_BITS  ] = 1'b1;
                {8'hxx,`KB_PONE_LEFT  }: pone_nxt[`KB_PONE_LEFT_BITS  ] = 1'b1;
                {8'hxx,`KB_PONE_RIGHT }: pone_nxt[`KB_PONE_RIGHT_BITS ] = 1'b1;
                {8'hxx,`KB_PONE_BOMB  }: pone_nxt[`KB_PONE_BOMB_BITS  ] = 1'b1;
                {8'hxx,`KB_PTWO_UP    }: ptwo_nxt[`KB_PTWO_UP_BITS    ] = 1'b1;
                {8'hxx,`KB_PTWO_DOWN  }: ptwo_nxt[`KB_PTWO_DOWN_BITS  ] = 1'b1;
                {8'hxx,`KB_PTWO_LEFT  }: ptwo_nxt[`KB_PTWO_LEFT_BITS  ] = 1'b1;
                {8'hxx,`KB_PTWO_RIGHT }: ptwo_nxt[`KB_PTWO_RIGHT_BITS ] = 1'b1;
                {8'hxx,`KB_PTWO_BOMB  }: ptwo_nxt[`KB_PTWO_BOMB_BITS  ] = 1'b1;     
                {8'hxx,`KB_MENU_CANCEL}: menu_nxt[`KB_MENU_CANCEL_BITS] = 1'b1;
                {8'hxx,`KB_MENU_OK    }: menu_nxt[`KB_MENU_OK_BITS    ] = 1'b1;
                {8'hxx,`KB_MENU_UP    }: menu_nxt[`KB_MENU_UP_BITS    ] = 1'b1;
                {8'hxx,`KB_MENU_DOWN  }: menu_nxt[`KB_MENU_DOWN_BITS  ] = 1'b1;
                
                default:;
            endcase
        end
    end

endmodule
