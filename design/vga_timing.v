//////////////////////////////////////////////////////////////////////////////////
// Company:     AGH UST
// Author:      DW
// 
// Create Date: 06.04.2018 18:48:21
// Design Name: uBomberman
// Module Name: vga_timing
// Project Name: uec2_project
// Description: This is the module for generating timing signals for VGA.
// 
// Dependencies: 
// none
//
// Revisions:
// - Rev. 1.0   -DW- File Created
// 
//////////////////////////////////////////////////////////////////////////////////

`timescale 1 ns / 1 ps
`include "_vga_macros.vh"

module vga_timing (
    input  wire        clk,
    input  wire        rst,
    output wire [`VGA_BUS_SIZE-1:0] vga_out
    );

    `VGA_OUT_WIRE
    `VGA_MERGE_OUTPUT(vga_out)
      
    //horizontal counter clock enable
    wire henable;
    assign henable = 1;
    
    //horizontal counter
    vga_counter 
        #( .MAX_VALUE(`H_MAX_VALUE),
           .BLNK_START(`H_BLNK_START),
           .SYNC_START(`H_SYNC_START),
           .SYNC_END(`H_SYNC_END)
        )
    vga_h_counter(
        .clk(clk),
        .rst(rst),
        .enable(henable),
        .count(hcount_out),
        .blnk(hblnk_out),
        .sync(hsync_out)
    );

    //clock enable for vertical counter
    wire venable;
    assign venable = (hcount_out == `H_MAX_VALUE);
    
    //vertical counter
    vga_counter 
        #( .MAX_VALUE(`V_MAX_VALUE),
           .BLNK_START(`V_BLNK_START),
           .SYNC_START(`V_SYNC_START),
           .SYNC_END(`V_SYNC_END)
         )
    vga_v_counter(
        .clk(clk),
        .rst(rst),
        .enable(venable),
        .count(vcount_out),
        .blnk(vblnk_out),
        .sync(vsync_out)
    );

endmodule
