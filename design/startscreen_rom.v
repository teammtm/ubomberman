//////////////////////////////////////////////////////////////////////////////////
// Company:     AGH UST
// Author:      DW
// 
// Create Date: 06.04.2018 20:37:46
// Design Name: uBomberman
// Module Name: startscreen_rom
// Project Name: uec2_project
// Description: This is the module for generating background for start screen.
// 
// Dependencies: 
// - startscreen.data
//
// Revisions:
// - Rev. 1.0   -DW- File Created
// 
//////////////////////////////////////////////////////////////////////////////////

module startscreen_rom (
    input wire clk ,
    input wire [14:0] address,  // address = {addry[5:0], addrx[8:0]}
    output reg [11:0] rgb
);

reg [11:0] rom [0:32767];

initial $readmemh("/startscreen.data", rom); 

always @(posedge clk)
    rgb <= rom[address];

endmodule
