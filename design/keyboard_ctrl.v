`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: Digilent Inc 
// Engineer: Arthur Brown
// Modyfied by: Dawid Włodarczyk, AGH UST
// 
// Create Date: 07/27/2016 02:04:01 PM
// Design Name: Basys3 Keyboard Demo
// Module Name: keyboard_ctrl
// Project Name: Keyboard
// Target Devices: Basys3
// Tool Versions: 2016.X
// Description: 
//     Receives input from USB-HID in the form of a PS/2, gives keyboard key presses and releases at output.
// Dependencies: 
//     _keyboard_macros, PS2Receiver, decode_keycode
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
//     Known issue, when multiple buttons are pressed and one is released, the scan code of the one still held down is ometimes re-sent.
// 
// Rev. 1.00    -DW- 01.06.2018 File modified for the needs of uec2_project - uBomberman design:
//                                  - unused lines commented
//                                  - added decode_keycode module
//                                  - modified inputs and outputs of module: clk50MHz, rst, kb_bus_out 
//                                  - added reset (rst) to PS2Receiver module
//
//////////////////////////////////////////////////////////////////////////////////
`include "_keyboard_macros.vh"

module keyboard_ctrl(
    input  wire                    clk,
    input  wire                    clk50MHz,
    input  wire                    rst,
    input  wire                    PS2Data,
    input  wire                    PS2Clk,
    output wire [`KB_BUS_SIZE-1:0] kb_bus_out
);

    wire [15:0] keycode;
    wire        flag;    
//    wire        tready;
//    wire        ready;
//    wire        tstart;
//    reg         start=0;
//    reg         CLK50MHZ=0;
//    wire [31:0] tbuf;
//    reg  [15:0] keycodev=0;
//    wire [ 7:0] tbus;
//    reg  [ 2:0] bcount=0;
//    reg         cn=0;
    
//    always @(posedge clk)
//        if(rst)
//            CLK50MHZ <= 0;
//        else
//            CLK50MHZ<=~CLK50MHZ;

    PS2Receiver uut (
        .clk(clk50MHz),
        .kclk(PS2Clk),
        .kdata(PS2Data),
        .keycode(keycode),
        .oflag(flag)
    );
    
    decode_keycode my_decode_keycode (
        .clk(clk),
        .rst(rst),
        .keycode(keycode),
        .flag(flag),
        .kb_bus_out(kb_bus_out)
    );
    
//    always@(keycode)
//        if (keycode[7:0] == 8'hf0) begin
//            cn <= 1'b0;
//            bcount <= 3'd0;
//        end else if (keycode[15:8] == 8'hf0) begin
//            cn <= keycode != keycodev;
//            bcount <= 3'd5;
//        end else begin
//            cn <= keycode[7:0] != keycodev[7:0] || keycodev[15:8] == 8'hf0;
//            bcount <= 3'd2;
//        end
    
//    always@(posedge clk)
//        if (flag == 1'b1 && cn == 1'b1) begin
//            start <= 1'b1;
//            keycodev <= keycode;
//        end else
//            start <= 1'b0;
     
//    bin2ascii #(
//        .NBYTES(2)
//    ) conv (
//        .I(keycodev),
//        .O(tbuf)
//    );
    
//    uart_buf_con tx_con (
//        .clk    (clk   ),
//        .bcount (bcount),
//        .tbuf   (tbuf  ),  
//        .start  (start ), 
//        .ready  (ready ), 
//        .tstart (tstart),
//        .tready (tready),
//        .tbus   (tbus  )
//    );
    
//    uart_tx get_tx (
//        .clk    (clk),
//        .start  (tstart),
//        .tbus   (tbus),
//        .tx     (tx),
//        .ready  (tready)
//    );
    
endmodule
