//////////////////////////////////////////////////////////////////////////////////
// Company:     AGH UST
// Author:      DW
// 
// Create Date: 06.04.2018 18:59:02
// Design Name: uBomberman
// Module Name: vga_counter
// Project Name: uec2_project
// Description: This is the counter module for VGA.
// 
// Dependencies: 
// none
//
// Revisions:
// - Rev. 1.0   -DW- File Created
// 
//////////////////////////////////////////////////////////////////////////////////

`timescale 1ns / 1ps

module vga_counter
    #( parameter
        MAX_VALUE = 2048,
        BLNK_START = 1,
        SYNC_START = 1,
        SYNC_END = 1
    )
    (
    input  wire        clk,
    input  wire        rst,
    input  wire        enable,
    output reg  [10:0] count,
    output reg         blnk,
    output reg         sync
    );
    
    reg  [10:0] count_nxt;
    
    always @(posedge clk)
            if (enable)
                count <= #1 count_nxt;
            else
                count <= #1 count;
    
    always @*
        if(rst)
            count_nxt = 0;
        else if(count == MAX_VALUE)
            count_nxt = 0;
        else
            count_nxt = count+1;
    
    reg blnk_nxt;
    reg sync_nxt;
    
    always @(posedge clk)
        if (enable)
            begin
                blnk <= #1 blnk_nxt;
                sync <= #1 sync_nxt;
            end
        else
            begin
                blnk <= #1 blnk;
                sync <= #1 sync;
            end
    
    always @*
        if(rst)
            begin
                blnk_nxt = 0;
                sync_nxt = 0;
            end
        else
            begin
                blnk_nxt = (count_nxt >= BLNK_START);
                sync_nxt = ((count_nxt >= SYNC_START) && (count_nxt <= SYNC_END));
            end
     
endmodule
