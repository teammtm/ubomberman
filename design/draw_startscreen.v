//////////////////////////////////////////////////////////////////////////////////
// Company:     AGH UST
// Author:      DW
// 
// Create Date: 06.04.2018 19:44:13
// Design Name: uBomberman
// Module Name: draw_startscreen
// Project Name: uec2_project
// Description: This is the module for generating background for start screen.
// 
// Dependencies: 
// none
//
// Revisions:
// - Rev. 1.0   -DW- File Created
// 
//////////////////////////////////////////////////////////////////////////////////
`timescale 1ns / 1ps
`include "_vga_macros.vh"

module draw_startscreen(
    input  wire        clk,
    input  wire        rst,
    input  wire [`VGA_BUS_SIZE-1:0] vga_in,
    input  wire [11:0] rgb_in,
    input  wire [11:0] rgb_pixel,
    output wire [`VGA_BUS_SIZE-1:0] vga_out,
    output wire [11:0] rgb_out,
    output reg  [14:0] pixel_addr
    );
    
    `VGA_SPLIT_INPUT(vga_in)
    `VGA_OUT_WIRE
    `VGA_MERGE_OUTPUT(vga_out)
    
    reg  [35:0] rgb;
    reg  [35:0] rgb_nxt;

    reg  [2:0]  hsync, vsync;
    reg  [2:0]  hblnk, vblnk;
    reg  [32:0] hcount, vcount;
    
    reg  [2:0]  hsync_nxt, vsync_nxt;
    reg  [2:0]  hblnk_nxt, vblnk_nxt;
    reg  [32:0] hcount_nxt, vcount_nxt;
    
    reg  [14:0] pixel_addr_nxt;
    wire [8:0] xaddr;
    wire [5:0] yaddr;
    
    always @(posedge clk)
        begin
            hcount  <= hcount_nxt;
            hsync   <= hsync_nxt;
            hblnk   <= hblnk_nxt;
            vcount  <= vcount_nxt;
            vsync   <= vsync_nxt;
            vblnk   <= vblnk_nxt;
            rgb     <= rgb_nxt;
            pixel_addr <= pixel_addr_nxt;
        end

    assign hcount_out = hcount[32:22];
    assign hsync_out = hsync[2];
    assign hblnk_out = hblnk[2];

    assign vcount_out = vcount[32:22];
    assign vsync_out = vsync[2];
    assign vblnk_out = vblnk[2];

    assign rgb_out = rgb[35:24];

    assign xaddr = (hcount_in[9:1]);// - (xpos_in[10:0]));
    assign yaddr = (vcount_in[9:4]);// - (ypos_in[10:0]));
    
    always @*
        if(rst)
            begin
                pixel_addr_nxt = 0;
                hcount_nxt = 0; 
                hsync_nxt  = 0; 
                hblnk_nxt  = 0; 
                vcount_nxt = 0; 
                vsync_nxt  = 0;
                vblnk_nxt  = 0;
                rgb_nxt  = 0;
            end
        else
            begin
                pixel_addr_nxt = {yaddr,xaddr};
                hcount_nxt     = {hcount[21:11],hcount[10:0],hcount_in}; 
                hsync_nxt      = {hsync[1],hsync[0],hsync_in}; 
                hblnk_nxt      = {hblnk[1],hblnk[0],hblnk_in}; 
                vcount_nxt     = {vcount[21:11],vcount[10:0],vcount_in}; 
                vsync_nxt      = {vsync[1],vsync[0],vsync_in};
                vblnk_nxt      = {vblnk[1],vblnk[0],vblnk_in};
                rgb_nxt [23:0] = {rgb[11:0],rgb_in};
                
                //if ((hcount[21:11] >= 0) && (hcount[21:11]< (490)) && (vcount[21:11] >= 0) && (vcount[21:11] < (64)))
                    rgb_nxt[35:24] = rgb_pixel;
                //else
                    //rgb_nxt[35:24] = rgb[23:12];
            end
endmodule
